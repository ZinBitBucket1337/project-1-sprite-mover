﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

    // Recognition that there is a Rigidbody2D with the game object.
    Rigidbody2D rb;
    // This allows the designer to modify the speed of the sprite.
    public float speed;
    // This allows the p button to pause and unpause by setting it to a boolean.
    public bool pause = false;

	void Awake () 
    {
        // Variable declaring that there is an attachment between the game object and the component of a Rigidbody2D.
        rb = GetComponent<Rigidbody2D>();
	}
	
	// This block of code allows for movement by allowing the user to controll where they want to go by using the WASD buttons on the keyboard or the arrows on the keyboard.
	void Update () 
    {
        // This line of code allows the user to move the sprite left or right.
        rb.AddForce(new Vector2(Input.GetAxis("Horizontal") * speed, 0));
        // This line of code allows the user to move the sprite up or down.
        rb.AddForce(new Vector2(0, Input.GetAxis("Vertical") * speed));
        // This line of code sets the button q to make everything inactive.
        if (Input.GetKeyDown("q"))
        {
            Time.timeScale = 0;
            speed = 0;
        }
        // This line of code sets the button escape to exit the game.
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        // This line of code sets the space button to reset the position to the original position.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.localPosition = new Vector3(0, 0, 0);
        }
        // This if else statement sets the p button to pause and unpause the game.
        if (Input.GetKeyDown("p"))
        {
            if (pause)
                Time.timeScale = 1;
            else
                Time.timeScale = 0;

            pause = !pause;
        }
        // This if statement allows the user to press left shift and the up arrow to move one unit up.
        if ((Input.GetKeyDown(KeyCode.UpArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.UpArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(0.0f, 1.0f, 0.0f);
        }
        // This if statement allows the user to press left shift and the down arrow to move one unit down.
        if ((Input.GetKeyDown(KeyCode.DownArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.DownArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(0.0f, -1.0f, 0.0f);
        }
        // This if statement allows the user to press the left shift and the left arrow to move one unit left.
        if ((Input.GetKeyDown(KeyCode.LeftArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.LeftArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(-1.0f, 0.0f, 0.0f);
        }
        // This if statement allows the user to press the left shift and the right arrow to move one unit right.
        if ((Input.GetKeyDown(KeyCode.RightArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.RightArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(1.0f, 0.0f, 0.0f);
        }
    }
}
